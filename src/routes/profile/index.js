import { h, Component } from 'preact';
import style from './style';
import { GoogleCharts } from 'google-charts';
import {Cube} from 'styled-loaders';
import cls from './style.css';

export default class Profile extends Component {
	emotions = [];
	eventSource;

	state = {
		msg: null,
		loading: true,
	};

	componentDidMount() {
		let eventSource = new EventSource('http://localhost:5000/event-stream');
		eventSource.onmessage = (msg) => {
			try {
				let data = JSON.parse(msg.data);
				this.setState({loading: false});
				setTimeout(() => GoogleCharts.load(() => this.drawChart(data)), 2000);
			} catch(e) {
				console.log("eroare", e);
			}

		}
	}
	render(props, state, context) {
		return state.loading ? <div class={cls.loader}><Cube color="#6d3580"/></div> : <div id="chart" class={style.profile}></div>;
	}


	drawChart(datax) {
		this.emotions.push(datax);
		console.log("data", this.emotions);
		let chartData = this.emotions.map(item => {
			let emotionLevels = (Object.values(item).map(val => {
				switch (val) {
					case 'VERY_LIKELY': return 5;
					case 'LIKELY': return 4;
					case 'POSSIBLE': return 3;
					case 'UNLIKELY': return 2;
					case 'VERY_UNLIKELY': return 1;
					default: return 2;
				}
			}));
			emotionLevels.unshift('');
			return emotionLevels;
		});
		chartData.unshift(['', 'Joy', 'Sorrow', 'Anger', 'Surprise']);
		console.log(chartData);
		var data = GoogleCharts.api.visualization.arrayToDataTable(chartData);

		var options = {
			title: 'Realtime Emotions',
			curveType: 'function',
			legend: { position: 'bottom' },
			vAxis: {
				title: 'Emotion Intensity'
			},
			tooltip: {trigger: 'selection'},
			axisTitlesPosition: 'none',
			theme: 'material',
		};

		var chart = new GoogleCharts.api.visualization.LineChart(document.getElementById('chart'));


		chart.draw(data, options);
	}
}
