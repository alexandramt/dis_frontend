import { h, Component } from 'preact';
import axios from 'axios';
import sortBy from 'lodash/sortBy';
import groupBy from 'lodash/groupBy';
import filter from 'lodash/filter';
import forOwn from 'lodash/forOwn';
import { GoogleCharts } from 'google-charts';
import {Cube} from 'styled-loaders';

import cls from './style.scss';

export default class Logs extends Component {
	state = {
		loading: true,
	};

	questions = [];

	componentDidMount() {
		axios.get('http://localhost:5000/logs').then(res => {
			let data = res.data;
			data.pop();
			data = groupBy(sortBy(data.map(item => JSON.parse(item)), 'timestamp'), 'question');
			console.log(data);
			forOwn(data, (value, key) => {
				let mistakes = 0;
				value.forEach((item, index) => {
					if (index < value.length - 1) {
						if ((item.tag === 'correct' && value[index + 1].tag !== 'next')
						|| (item.tag === 'wrong' && value[index + 1].tag !== 'retry')
						|| (item.tag === 'answer' && value[index + 1].tag === 'answer')) {
							mistakes++;
						}
					}
				});
				let tries = value.length;
				// let correctNext = filter(value, (item, index) => {
				// 	return item.tag === 'correct' && value[index+1].tag === 'next'
				// }).length;
				// let correctRetry = filter(value, (item, index) => {
				// 	return item.tag === 'wrong' && value[index+1].tag === 'retry'
				// }).length;
				this.questions[key] = ['Question ' + (parseInt(key) + 1), tries - 1 - mistakes, mistakes];
			});
			this.setState({loading: false});
			GoogleCharts.load(() => this.drawChart());
		}).catch(e => console.log(e));
	}

	render(props, state, context) {
		return state.loading ? <Cube class={cls.loader} color='#6d3580'/> : <div class={cls.logs} id="columnChart"></div>
	}

	drawChart() {
		this.questions.unshift(['Question', 'Correct tries', 'Wrong tries']);
		console.log(this.questions);

		var data = GoogleCharts.api.visualization.arrayToDataTable(this.questions);

		var options = {
			theme: 'material',
			bars: 'vertical',
			bar: {
				groupWidth: '25%',
			},
			colors: ['#69C08A', '#F40E0E']
		};


		var chart = new GoogleCharts.api.visualization.BarChart(document.getElementById('columnChart'));


		chart.draw(data, options);
	}
}