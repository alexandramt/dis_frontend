import { h } from 'preact';
import style from './style';
import PieCharts from "../../components/pie-charts";

const Home = () => (
	<div class={style.home}>
		<PieCharts/>
	</div>
);

export default Home;
