import { h } from 'preact';
import { Link } from 'preact-router/match';
import style from './style';

const Header = () => (
	<header class={style.header}>
		<nav>
			<Link activeClassName={style.active} href="/">Case Study Results</Link>
			<Link activeClassName={style.active} href="/profile">Real-time Execution</Link>
			<Link activeClassName={style.active} href="/logs">Log Analysis</Link>
		</nav>
	</header>
);

export default Header;
