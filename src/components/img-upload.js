import { Component } from 'preact';
import axios from 'axios';
import cls from './img-upload.scss';

export default class ImgUpload extends Component {

	render() {
		{/*<form id="uploadbanner" encType="multipart/form-data" method="post" action="#">*/}
			{/*<input id="fileupload" name="myfile" type="file"/>*/}
			{/*<input type="submit" value="submit" id="submit"/>*/}
		{/*</form>*/}
		return <div class={cls.container} id="uploadbanner" encType="multipart/form-data">
			<label for="avatar">Upload image:</label>
			<input type="file" class={cls.imgInput}
				   id="avatar" name="avatar"
				   onChange={this.analyzeImage}
				   accept="image/png, image/jpeg"/>
		</div>;
	}

	analyzeImage(e) {
		axios.get('http://localhost:5000/stats').then(res => {
			console.log(res);
		}).catch(e => console.log(e));
		console.log(e.target.files);
	}
}