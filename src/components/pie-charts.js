import { Component } from 'preact';
import axios from 'axios';
import { GoogleCharts } from 'google-charts';
import {Cube} from 'styled-loaders';

import cls from './pie-charts.scss';

export default class PieCharts extends Component {

	state = {
		loadingFace: true,
		loadingLabel: true,
	};

	componentDidMount() {
		axios.get('http://localhost:5000/faceStats').then(res => {
			this.setState({loadingFace: false});
			GoogleCharts.load(() => this.drawChart(res));
		}).catch(e => console.log(e));
		axios.get('http://localhost:5000/labelStats').then(res => {
			this.setState({loadingLabel: false});
			GoogleCharts.load(() => this.drawSmileChart(res.data));
		}).catch(e => console.log(e));
	}

	render(props, state) {
		return 	(state.loadingFace && state.loadingLabel) ?
			<div class={cls.loader}><Cube color="#6d3580"/></div> :
			<div class={cls.charts}>
				<div id="piechart" style="width: 750px; height: 600px"></div>
				<div id="chart2" style="width: 750px; height: 600px"></div>
			</div>;
	}

	filterByEmotion(data, emotion) {
		return data.filter(item => item.length > 0 && item[0].joyLikelihood && item[0].joyLikelihood === emotion);
	}

	filterByLabel(data, label) {
		return data.filter(itemArray => itemArray.find(item => item.description === label));
	}

	drawSmileChart(res) {
		let smiles = this.filterByLabel(res, 'Smile');
		var data = GoogleCharts.api.visualization.arrayToDataTable([
			['Pac Man', 'Percentage'],
			['Smile', smiles.length],
			['Smile2', 100 - smiles.length]
		]);

		var options = {
			title: 'IMAGES LABELED WITH SMILE',
			titleTextStyle: {
				color: '#6d3580',
				textAlign: 'center',
				fontSize: 18
			},
			legend: 'none',
			pieSliceText: 'none',
			pieStartAngle: 135,
			slices: {
				0: { color: '#F7B32B' },
				1: { color: 'transparent' }
			}
		};

		var chart = new GoogleCharts.api.visualization.PieChart(document.getElementById('chart2'));
		chart.draw(data, options);
	}

	drawChart(res) {
		let possible = this.filterByEmotion(res.data, 'POSSIBLE');
		let very_likely = this.filterByEmotion(res.data, 'VERY_LIKELY');
		let likely = this.filterByEmotion(res.data, 'LIKELY');
		let unlikely = this.filterByEmotion(res.data, 'UNLIKELY');
		let very_unlikely = this.filterByEmotion(res.data, 'VERY_UNLIKELY');

		var data = GoogleCharts.api.visualization.arrayToDataTable([
			['Joy', 'Amount'],
			['Very likely', very_likely.length],
			['Likely', likely.length],
			['Possibly',  possible.length],
			['Unlikely', unlikely.length],
			['Very Unlikely', very_unlikely.length]
		]);

		var options = {
			title: 'JOY LIKELIHOOD',
			titleTextStyle: {
				color: '#6d3580',
				textAlign: 'center',
				fontSize: 18
			},
			legend: 'none',
			pieSliceText: 'label',
			pieStartAngle: 100,
			slices: {
				1: {
					offset: 0.2,
					color: '#E6A804'
				},
				3: {
					offset: 0.3,
					color: '#D2BAB9'
				},
				4: {
					color: '#cc4165'
				},
				5: {offset: 0.4,
					color: '#49094A'
				},
				2: {
					offset: 0.5,
					color: '#4CA885'
				},
				6: {
					color: '#A9E5BB'
				}
			},
		};

		var chart = new GoogleCharts.api.visualization.PieChart(document.getElementById('piechart'));

		chart.draw(data, options);
	}
}